package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        int round = 0;
        while (true) {
            round += 1;
            System.out.println("Let's play round " + round);
            String choice = "";
            while (true) {
                choice = readInput("Your choice (Rock/Paper/Scissors)?");
                if (choice.equals("rock") || choice.equals("paper") || choice.equals("scissors")) {
                    break;
                }
                else {
                    System.out.println("I do not understand " + choice + ". Could you try again?");
                }
            }
            // randomly create opponents move
            int rand = (int)(Math.random() * 3);
            String CPU = "";
            if (rand == 0) {
                CPU = "rock";
            } else if (rand == 1) {
                CPU = "paper";
            }  else {
                CPU = "scissors";
            }

            // print winner
            if (Objects.equals(choice, CPU)) {
                System.out.println("Human chose " + choice + ", computer chose " + CPU + ". It's a tie!");
            }
            else if ((choice.equals("rock") && CPU.equals("paper") || (choice.equals("scissors") && CPU.equals("rock"))) || choice.equals("paper") && CPU.equals("scissors")) {
                System.out.println("Human chose " + choice + ", computer chose " + CPU + ". Computer wins!");
                computerScore ++;

            }  else if ((CPU.equals("rock") && choice.equals("paper") || (CPU.equals("scissors") && choice.equals("rock"))) || CPU.equals("paper") && choice.equals("scissors")) {
                System.out.println("Human chose " + choice + ", computer chose " + CPU + ". Human wins!");
                humanScore ++;
            }

            System.out.println("Score: human " + humanScore + "," + " computer " + computerScore);
            String stay = readInput("Do you wish to continue playing? (y/n)?");
            if (Objects.equals(stay, "n")) {
                break;
            } else if (Objects.equals(stay, "y")) {
                continue;
            }  else {
                System.out.println("Unknown command, let us play again!");
            }
        }
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
